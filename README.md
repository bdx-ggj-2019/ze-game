# GGJ2020 - Game (*temporary name*)

# How To Build
You need to install [CMake](https://cmake.org/) and [SFML](https://www.sfml-dev.org/).

##Unix
Check your package manager for SFML. It's available on debian, redhat, mac, (etc...).
##Windows
If you're on windows, you may need to define the `SFML_ROOT` to 
your SFML cmake directory (`cmake .. -D "SFML_ROOT=SFML/lib/cmake/SFML/"`).

You also need to put the dlls by hand.

Dependencies:
- openal32.dll
- sfml-audio-2.dll
- sfml-audio-d-2.dll
- sfml-graphics-2.dll
- sfml-graphics-d-2.dll
- sfml-network-2.dll
- sfml-network-d-2.dll
- sfml-system-2.dll
- sfml-system-d-2.dll
- sfml-window-2.dll
- sfml-window-d-2.dll

They can all be found in `SFML/bin/`